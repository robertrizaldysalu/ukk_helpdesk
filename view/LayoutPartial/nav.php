<nav class="side-menu">
	    <ul class="side-menu-list">
	        <li class="red">
	            <a href="../Home/">
	                <i class="font-icon glyphicon glyphicon-send"></i>
	                <span class="lbl">Home</span>
	            </a>
	        </li>
	        <li class="blue-dirty">
	            <a href="../TiketKonsultasi">
	                <span class="glyphicon glyphicon-th"></span>
	                <span class="lbl">Konsultasi</span>
	            </a>
	        </li>
	       
	        <li class="pink-red">
	            <a href="../TiketBaru/">
	                <i class="font-icon font-icon-zigzag"></i>
	                <span class="lbl">Tiket Baru</span>
	            </a>
	        </li>
	      
	    </ul>
	
	 
	</nav><!--.side-menu-->