<?php
  require_once("../../config/Connect.php");
  if(isset($_SESSION["user_id"])){
?>

<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Tiket Konsultasi</title>

    <!-- link.php -->
    <?php require_once("../LayoutPartial/link.php"); ?>

	
</head>
<body class="with-side-menu">


    <!-- header.php -->
    <?php require_once("../LayoutPartial/header.php"); ?>

	<div class="mobile-menu-left-overlay"></div>

    <!-- nav.php -->
    <?php require_once("../LayoutPartial/nav.php"); ?>

	<div class="page-content">
		<div class="container-fluid">
			Blank page.
		</div><!--.container-fluid-->
	</div><!--.page-content-->

    <?php require_once("../LayoutPartial/script.php"); ?>
	
</body>
</html>

<?php
  }else{
	  header("Location: ".Connect::base_url()."index.php");
  }
  ?>